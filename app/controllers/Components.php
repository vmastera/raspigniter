<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."models/Entity/ComponentsEntity.php");
require_once(APPPATH."models/Entity/RoomsEntity.php");
use Entity\RoomsEntity;
use Entity\ComponentsEntity;

class Components extends MY_Controller {

	public function index(){
		$components = $this->loadRecords();
		$rooms = $this->loadRooms();
		$this->load->view('components',array('components'=>$components,'rooms'=>$rooms));
	}

	public function loadRecords(){
		return $this->doctrine->em
			->getRepository('Entity\ComponentsEntity')
			->createQueryBuilder('e')
			->select('e')
			->getQuery()
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}

	public function loadRooms(){
		return $this->doctrine->em
			->getRepository('Entity\RoomsEntity')
			->createQueryBuilder('e')
			->select('e')
			->getQuery()
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}

	public function addComponent(){
		if(!empty($this->input->post())) {
			$component = new Entity\ComponentsEntity;
			$component->setName($this->input->post('name'));
			$component->setRoomid($this->input->post('roomid'));
			$component->setPinNumber($this->input->post('pinNumber'));
			$component->setType($this->input->post('type'));
			$component->setDescription($this->input->post('description'));
			$this->doctrine->em->persist($component);
			$this->doctrine->em->flush();
			$this->doctrine->em->clear();
		}
		header("Location: ".$this->config->base_url()."components/");
	}

	public function updateComponent(){
		if($this->uri->segment(3)) {
			$component = $this->doctrine->em->getRepository('Entity\ComponentsEntity')->find($this->uri->segment(3));

			if (!$component) {
				throw $this->createNotFoundException('No guest found for id '.$this->uri->segment(3));
			}

			$component->setName($this->input->post('name'));
			$component->setRoomid($this->input->post('roomid'));
			$component->setPinNumber($this->input->post('pinNumber'));
			$component->setType($this->input->post('type'));
			$component->setDescription($this->input->post('description'));

			$this->doctrine->em->persist($component);
			$this->doctrine->em->flush();
		}
		header("Location: ".$this->config->base_url()."components/");
	}

	public function deleteComponent(){
		if($this->uri->segment(3)) {
			$room = $this->doctrine->em->getRepository('Entity\ComponentsEntity')->find($this->uri->segment(3));

			if (!$room) {
				throw $this->createNotFoundException('No guest found for id '.$this->uri->segment(3));
			}

			$this->doctrine->em->remove($room);
			$this->doctrine->em->flush();
		}
		header("Location: ".$this->config->base_url()."components/");
	}

}
