<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."models/Entity/UsersEntity.php");
require_once(APPPATH."models/Entity/GroupsEntity.php");
use Entity\UsersEntity;
use Entity\GroupsEntity;

class Auth extends CI_Controller {

	public function index(){
		$data = array();
		if(!empty($this->input->post())) {
			$data['message'] = $this->doLogin();
		}
		$this->load->view('login',$data);
		//var_dump($this->doctrine->em);
	}

	private function doLogin(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_checkCreds');

		//var_dump($this->form_validation->run());
		if($this->form_validation->run() == FALSE) {
			return validation_errors();
			//Field validation failed.  User redirected to login page
			//$this->load->view('login');
		} else {
			redirect('/dashboard');
		}
	}

	public function checkCreds()
	{
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		//query the database
		$query = $this->doctrine->em->
		createQuery('SELECT u FROM Entity\UsersEntity u WHERE u.username = :username AND u.password = :password')
			->setParameter('username', $username)
			->setParameter('password', $password)
			->setMaxResults(1)
		;

		$result = $query->getResult();

		$check = false;
		if(count($result) == 1)$check = true;

		if($check)
		{
			$session_array = array();
			foreach($result as $row)
			{
				//var_dump($row);
				$session_array = array(
					'id' => $row->getID(),
					'username' => $row->getUsername()
				);
				$this->session->set_userdata('logged_in', $session_array);
			}
			return true;
		} else {
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}

	public function logout(){
		$this->session->unset_userdata('logged_in');
		redirect('/');
	}
}
