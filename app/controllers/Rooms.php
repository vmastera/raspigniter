<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."models/Entity/RoomsEntity.php");
use Entity\RoomsEntity;

class Rooms extends MY_Controller {

	public function index(){
		$rooms = $this->loadRecords();
		$this->load->view('rooms',array('rooms'=>$rooms));
	}

	public function addRoom(){
		if(!empty($this->input->post())) {
			$room = new Entity\RoomsEntity;
			$room->setRoomname($this->input->post('name'));
			$room->setDescription($this->input->post('description'));
			$this->doctrine->em->persist($room);
			$this->doctrine->em->flush();
			$this->doctrine->em->clear();
		}
		header("Location: ".$this->config->base_url()."rooms/");
	}

	public function loadRecords(){
		//$query = $this->doctrine->em->createQuery("SELECT r FROM Rooms r");
		//return $query->getResult();
		//return $this->doctrine->em->getRepository("Entity\RoomsEntity")->findAll();
		return $this->doctrine->em
			->getRepository('Entity\RoomsEntity')
			->createQueryBuilder('e')
			->select('e')
			->getQuery()
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}

	public function deleteRoom(){
		if($this->uri->segment(3)) {
			$room = $this->doctrine->em->getRepository('Entity\RoomsEntity')->find($this->uri->segment(3));

			if (!$room) {
				throw $this->createNotFoundException('No guest found for id '.$this->uri->segment(3));
			}

			$this->doctrine->em->remove($room);
			$this->doctrine->em->flush();
		}
		header("Location: ".$this->config->base_url()."rooms/");
	}

	public function updateRoom(){
		if($this->uri->segment(3)) {
			$room = $this->doctrine->em->getRepository('Entity\RoomsEntity')->find($this->uri->segment(3));

			if (!$room) {
				throw $this->createNotFoundException('No guest found for id '.$this->uri->segment(3));
			}

			$room->setRoomname($this->input->post('name'));
			$room->setDescription($this->input->post('description'));

			$this->doctrine->em->persist($room);
			$this->doctrine->em->flush();
		}
		header("Location: ".$this->config->base_url()."rooms/");
	}

}
