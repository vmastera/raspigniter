<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!--<div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url(); ?>/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>-->
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php
                $menuItems = array('dashboard'=>'fa fa-dashboard','rooms'=>'fa fa-home', 'components'=>'fa fa-cogs');
                foreach($menuItems as $key=>$value){
                    echo '<li ';
                            if($this->uri->segment(1)==$key) echo 'class="active"';
                    echo '>
                            <a href="'.base_url().$key.'">
                                <i class="'.$value.'"></i> <span>'.ucfirst($key).'</span>
                            </a>
                        </li>
                    ';
                }
            ?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Widgets</span>
                    <i class="fa fa-angle-left pull-right"></i>
                    <ul class="treeview-menu">
                        <?php
                        foreach($this->config->item('modules_locations') as $location) {
                            $modules = scandir($location, 1);
                            foreach ($modules as $module) {
                                if ($module == '.' || $module == '..') continue;
                                echo '
                                    <li>
                                        <a href="'.base_url().$module.'">
                                        <i class="fa fa-dashboard"></i>'.ucfirst($module).'
                                        </a>
                                    </li>
                                ';
                            }
                        }
                        ?>
                    </ul>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>UI Elements</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                    <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                    <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                    <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                    <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                </ul>
            </li>
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
