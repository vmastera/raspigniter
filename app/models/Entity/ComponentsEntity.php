<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="components")
 */
class ComponentsEntity{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=32, nullable=false)
     */
    protected $name;

    /**
     * @Column(type="string", length=32, nullable=false)
     * @ManyToOne(targetEntity="RoomsEntity")
     * @JoinColumn(name="roomid", referencedColumnName="id")
     */
    protected $roomid;

    /**
     * @Column(type="integer", length=32, nullable=false)
     */
    protected $pinNumber;

    /**
     * @Column(type="string", length=64, columnDefinition="ENUM('temperature','humidity','relay')")
     */
    protected $type;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRoomid()
    {
        return $this->roomid;
    }

    /**
     * @param mixed $roomid
     */
    public function setRoomid($roomid)
    {
        $this->roomid = $roomid;
    }

    /**
     * @return mixed
     */
    public function getPinNumber()
    {
        return $this->pinNumber;
    }

    /**
     * @param mixed $pinNumber
     */
    public function setPinNumber($pinNumber)
    {
        $this->pinNumber = $pinNumber;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


}
