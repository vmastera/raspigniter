<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="rooms")
 */
class RoomsEntity{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=32, unique=true, nullable=false)
     */
    protected $roomname;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $description;

    /**
     * @param mixed $roomname
     */
    public function setRoomname($roomname)
    {
        $this->roomname = $roomname;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}
