<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="gpio")
 */
class GpioEntity{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="integer", length=32, nullable=false)
     */
    protected $pinNumber;

    /**
     * @Column(type="string", length=64, columnDefinition="ENUM('out','in')")
     */
    protected $mode;

    /**
     * @Column(type="string", length=64, columnDefinition="ENUM('0','1')")
     */
    protected $output;

    /**
     * @Column(type="integer", length=32, nullable=false)
     */
    protected $gpioId;


}
