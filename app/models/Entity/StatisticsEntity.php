<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="statistics")
 */
class StatisticsEntity
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=32, nullable=false)
     */
    protected $type;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $value;

    /**
     * @Column(type="string", length=32, nullable=false)
     * @ManyToOne(targetEntity="RoomsEntity")
     * @JoinColumn(name="roomid", referencedColumnName="id")
     */
    protected $roomid;

    /**
     * @Column(name="year", columnDefinition="YEAR", nullable=false)
     */
    protected $year;

    /**
     * @Column(name="dateTime", columnDefinition="DATETIME", nullable=false)
     */
    protected $dateTime;

}
