#!/usr/bin/python

import RPi.GPIO as GPIO
import sys, getopt

def main(argv):
   port = ''
   try:
      opts, args = getopt.getopt(argv,"hp:",["pport="])
   except getopt.GetoptError:
      print 'turnOnRelay.py -p <port>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'turnOnRelay.py -p <port>'
         sys.exit()
      elif opt in ("-p", "--pport"):
         port = int(arg)
   print port
   GPIO.setmode(GPIO.BOARD)

   # init list with pin number
   GPIO.setup(port, GPIO.OUT)
   GPIO.output(port, GPIO.HIGH)

if __name__ == "__main__":
   main(sys.argv[1:])
   #GPIO.cleanup()
