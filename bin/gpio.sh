#!/bin/sh 
#
#
# GPIO-Pin  15 (OUT)
echo "15" >  /sys/class/gpio/export
chmod 777 -R /sys/class/gpio/gpio15
echo "out" > /sys/class/gpio/gpio15/direction
