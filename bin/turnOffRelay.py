#!/usr/bin/python

import RPi.GPIO as GPIO
import sys, getopt
import time

def main(argv):
   port = ''
   SleepTimeL = 2
   try:
      opts, args = getopt.getopt(argv,"hp:",["pport="])
   except getopt.GetoptError:
      print 'turnOnRelay.py -p <port>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'turnOnRelay.py -p <port>'
         sys.exit()
      elif opt in ("-p", "--pport"):
         port = int(arg)
   print port
   GPIO.setmode(GPIO.BOARD)
   GPIO.setwarnings(False)
   # init list with pin number
   GPIO.setup(port, GPIO.IN)
   #GPIO.output(port, GPIO.LOW)

if __name__ == "__main__":
   main(sys.argv[1:])
   GPIO.cleanup()
