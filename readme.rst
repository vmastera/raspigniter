###################
Upcoming fixes
###################
1) Finish Dashboard and move it to the main application, this should not stay into modules.
2) Create user login/logout logic and implement it.
3) Finish left and top menu functionality.
4) Finish GPIO module.


###################
What is RaspIgniter
###################

RaspIgniter is an Application Development Framework - a toolkit - for people
who wants to build smart home control panel using PHP.

*******************
Release Information
*******************

This repo contains in-development code for future releases. To download the
latest stable release please visit the `RaspIgniter Downloads
<http://www.raspigniter.com/download>`_ page.

**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`_.

*******************
Server Requirements
*******************
Apache2

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************
You can use install.sh script

Or you can do steps manually:

1) install apache,mysql and php
2) Enter into folder and run this comand './composer.phar update'
3) Enter credentials for mysql into app/config/database.php
4) Run Doctrine with this comand './doctrine orm:schema-tool:create'
5) enable mod_rewrite with 'a2enmod rewrite'
6) Fix vhost file, Here is an example vhost file:

   <VirtualHost *:80>
	DocumentRoot /var/www/
	<Directory /var/www/>
		Options FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>

	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
	<Directory "/usr/lib/cgi-bin">
		AllowOverride None
		Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		Order allow,deny
		Allow from all
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel emerg

	CustomLog ${APACHE_LOG_DIR}/access.log combined
   </VirtualHost>
7) Go to /var/www/bin/ and run 'make'
8) Install Mosquitto - sudo apt-get install mosquitto mosquitto-clients python-mosquitto

*******
License
*******



*********
Resources
*********


***************
Acknowledgement
***************