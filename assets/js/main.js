$( document ).ready(function() {
    $('.editRoom').click(function(e){
        e.preventDefault();
        var data = $(this).data();
        $("#editForm #editName").val(data.name);
        $("#editForm #editDescription").val(data.description);
        $("#editForm").attr('action', data.action);
    });

    $('.editComponent').click(function(e){
        e.preventDefault();
        var data = $(this).data();
        $("#editForm #editName").val(data.name);
        $("#editForm #editRoomid").val(data.roomid);
        $("#editForm #editType").val(data.type);
        $("#editForm #editPinNumber").val(data.pinnumber);
        $("#editForm #editDescription").val(data.description);
        $("#editForm").attr('action', data.action);
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $('#svg_4').on('click',function(){
        alert(123);
    });
});